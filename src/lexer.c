#include "lexer.h"
#include <stdbool.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

char* read_lexeme();

struct Lexer lexer;



struct CheckLexeme{
    bool found;
    struct Token t;

};

struct Machine{
    char current;

};




struct CheckLexeme isIf(char* string){
    struct CheckLexeme cl;
    cl.found = strcmp(string,"if");
    cl.t.type = IF;
    return cl;
}

struct CheckLexeme isFor(char* string){
    struct CheckLexeme cl;
    cl.found = strcmp(string, "for");
    cl.t.type = FOR;
    return cl;
}

struct CheckLexeme isWhile(char* string){
    struct CheckLexeme cl;
    cl.found = strcmp(string, "while");
    cl.t.type = WHILE;
    return cl;
}

struct CheckLexeme isUntil(char* string){
    struct CheckLexeme cl;
    cl.found = strcmp(string, "until");
    cl.t.type = UNTIL;
    return cl;
}

struct CheckLexeme isElse(char* string){
    struct CheckLexeme cl;
    cl.found = strcmp(string, "else");
    cl.t.type = ELSE;
    return cl;
}

struct CheckLexeme isLet(char* string){
    struct CheckLexeme cl;
    cl.found = strcmp(string, "let");
    cl.t.type = LET;
    return cl;
}

struct CheckLexeme isConst(char* string){
    struct CheckLexeme cl;
    cl.found = strcmp(string, "const");
    cl.t.type = CONST;
    return cl;
}

struct CheckLexeme isVar(char* string){
    struct CheckLexeme cl;
    cl.found = strcmp(string, "var");
    cl.t.type = VAR;
    return cl;
}

struct CheckLexeme isImport(char* string){
    struct CheckLexeme cl;
    cl.found = strcmp(string, "import");
    cl.t.type = IMPORT;
    return cl;
}

struct CheckLexeme isPackage(char* string){
    struct CheckLexeme cl;
    cl.found = strcmp(string,"package");
    cl.t.type = PACKAGE;
    return cl;
}

struct CheckLexeme isFunc(char* string){
    struct CheckLexeme cl;
    cl.found = strcmp(string, "func");
    cl.t.type = FUNC;
    return cl;

}

struct CheckLexeme isIdentifier(char* string){
    struct CheckLexeme cl;
    bool notIdentifier;
    for(int i=0;i<strlen(string);i++){
        if(i==0&&!isalpha(string[0])){
            notIdentifier = true;
            break;
        }else if(!isalnum(string[i])){
            notIdentifier = true;
            break;
        }
    }
    if(notIdentifier)
    {
        cl.found = false;
    }else{
        cl.found = true;
        cl.t.string = string;
        cl.t.type = IDENTIFIER;
    }

    return cl;
}

struct CheckLexeme isLeftBrace(char* string){
    struct CheckLexeme cl;
    cl.found = strcmp(string,"{");
    cl.t.type = LEFTBRACE;
    return cl;
}

struct CheckLexeme isRightBrace(char* string){
    struct CheckLexeme cl;
    cl.found = strcmp(string,"}");
    cl.t.type = RIGHTBRACE;
    return cl;
}

struct CheckLexeme is

struct Lexer lexer_init(FILE* input){
    lexer.source = input;
    return lexer;
}

struct Token next_token(){
    char* lexeme = read_lexeme();
    struct CheckLexeme state;
    
    state=isPackage(lexeme);
    state=isImport(lexeme);
    state=isVar(lexeme);
    state=isConst(lexeme);
    state=isElse(lexeme);
    state=isIf(lexeme);
    state=isFor(lexeme);
    state=isWhile(lexeme);
    state=isUntil(lexeme);
    
    return state.t;
}

char* read_lexeme(){
    char* string = malloc(sizeof(char));
    char c = 0;
    int size = 1;
    strcpy(string, "\0");
    while((c=fgetc(lexer.source))!='\n' && c!=' ')
    {
        size++;
        string = realloc(string, sizeof(char)*size);
        string[size-2] = c;       
    }
    return string;
}
