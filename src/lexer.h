#include <stdio.h>
#ifndef __LEXER_H__
#define __LEXER_H__

enum TokenType{
    IF,FOR,WHILE,UNTIL,ELSE,ELSEIF,LET,CONST,VAR,IMPORT,PACKAGE,
    EOF
};

struct Token{
    char* string;
    enum TokenType type;
};

struct Lexer{
    FILE* source;
    struct Token current;
};



struct Lexer lexer_init(FILE* source);
struct Token next_token();

#endif